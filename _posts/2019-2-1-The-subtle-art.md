---
layout: post
title: The Subtle Art of Not Giving a F*ck
image: /images/posts/subtle-art.jpg
tags: ["english"]
---

## Perpective
If there are two most important things I've got from this book, those will be:
- Choose to be responsible for things that happened in your life whether or not you are the cause of it.
- Action isn't just the effect of motivation, it's also the cause of it. So not only motivation -> action but also action -> motivation

## Book Summary

